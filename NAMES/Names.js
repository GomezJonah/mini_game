var temp = document.querySelector('.time');
 	var button = document.querySelector("button");
 	var words = document.querySelector(".words");
 	var timerDiv = document.querySelector(".time");
 	var scoreDiv = document.querySelector(".score");
 	var points = 0;
 	var spans;
 	var typed;
   var seconds = 60;
   

 	function countdown() {
 		points = 0;
 		var timer = setInterval(function(){
 			button.disabled = true;
    		seconds--;
    		temp.innerHTML = seconds;
    		if (seconds === 0) {
    			alert("Game over! Your score is " + points);
    			scoreDiv.innerHTML = "0";
    			words.innerHTML = "";
    			button.disabled = false;
    			clearInterval(timer);
    			seconds = 60;
    			timerDiv.innerHTML = "60";
    			button.disabled = false;	
    		}
 		}, 1000);
  	}

  	function random() {
  		words.innerHTML = "";
  		var random = Math.floor(Math.random() * (39)) + 0 ;
  		var wordArray = list[random].split("");
  		for (var i = 0; i < wordArray.length; i++) {
  			var span = document.createElement("span");
  			span.classList.add("span");
  			span.innerHTML = wordArray[i];
  			words.appendChild(span);
  		}
  		spans = document.querySelectorAll(".span");
  	}


  	const list = ['JHESSA','JET','MIYA','LESLY','CLINT','ZILONG','KARINA','KARIE','ANGELA','ODETTE',
    'XANDER','LORD','EUDORA','ARVIN','MARC','JOHNSON','NANA','ABCDEF','JAMES','ELIZABETH','FANNY','RAFAELA',
    'FORD','DANIEL','ELLE','LEA','ELLEN','CHRISTY','JHONAS','JEREMY','JANA','JANIKA' ,
    'JONAS','JULLIANE','STEPHEN','TIGER','CYRHEA','RHYAN','LAYLA'];

  	button.addEventListener("click", function(e){
  		countdown();
  		random();
  		button.disabled = true;	
  	});


  	function typing(e) {
  			typed = String.fromCharCode(e.which);
  			for (var i = 0; i < spans.length; i++) {
  				if (spans[i].innerHTML === typed) {
  					if (spans[i].classList.contains("bg")) { 
  						continue;
  					} else if (spans[i].classList.contains("bg") === false && spans[i-1] === undefined || spans[i-1].classList.contains("bg") !== false ) { // if it dont have class, if it is not first letter or if the letter before it dont have class (this is done to avoid marking the letters who are not in order for being checked, for example if you have two "A"s so to avoid marking both of them if the first one is at the index 0 and second at index 5 for example)
  						spans[i].classList.add("bg");
  						break;
  					}
  				}
  			}
  			var checker = 0;
  			for (var j = 0; j < spans.length; j++) { 
  				if (spans[j].className === "span bg") {
  					checker++;
  				}
  				if (checker === spans.length) { 
					  currentTime = 0;
  					points++; 
  					scoreDiv.innerHTML = points; 
  					document.removeEventListener("keydown", typing, false);
  					setTimeout(function(){
  						words.className = "words"; 
  						random(); 
  						document.addEventListener("keydown", typing, false);
  					}, 400);
  				}

  			}
  	}

    document.addEventListener("keydown", typing, false);
	
